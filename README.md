# Truphone.CodeChallenge

Technical coding challenge part of recruitment process by Truphone 

Contains the implementation of a rest API for management of a device's database.
Methods implemented:
  - Add (adds a new device to the DB)
  - Get (retieves a device from the database given its id)
  - List (returns all the devices in the DB)
  - Update (updates a device)
  - Delete (removes a device from the database)
  - Search (returns the devices of a given brand)


Requests examples:

# Add

curl -X POST 'https://localhost:54018/api/devices' ^
-H 'Content-Type: application/json' ^
--data-raw '    {
        "name": "Custodio",
        "brand": "Niko",
        "creationTime": "2020-12-13T17:12:41"
    }'

	
# List
curl -X GET 'https://localhost:54018/api/devices'


# Get
curl -X GET 'https://localhost:54018/api/devices/42'


# Search
curl -X GET 'https://localhost:54018/api/devices?brand=Niko'


# Update (Full)
curl -X PUT 'https://localhost:54018/api/devices' ^
-H 'Content-Type: application/json' ^
--data-raw '    {
        "brand": "LasCostas",
        "creationTime": "2021-02-23T12:22:41.0840387",
        "id": 40,
        "name": "Manuel"
    }'


# Update (Partial)
curl -X PATCH 'https://localhost:54018/api/devices' ^
-H 'Content-Type: application/json' ^
--data-raw '    {
        "id": 44,
        "name": "Amilcar"
    }'

	
# Delete
curl -X DELETE 'https://localhost:54018/api/devices/42'
