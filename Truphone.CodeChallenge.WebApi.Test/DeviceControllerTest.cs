using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Truphone.CodeChallenge.WebApi.Controllers;
using Truphone.CodeChallenge.WebApi.Data;
using Xunit;

namespace Truphone.CodeChallenge.WebApi.Test
{
    public class DeviceControllerTest
    {
        private readonly AutoMapper.IMapper _mapper;

        public DeviceControllerTest()
        {
            var mockMapper = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new Mapping.Mappings());
            });
            _mapper = mockMapper.CreateMapper();
        }

        [Fact]
        public async void Add_Success()
        {
            var options = new DbContextOptionsBuilder<DeviceDbContext>()
            .UseInMemoryDatabase(databaseName: "AddSuccessDevicesDb")
            .Options;

            using (var context = new DeviceDbContext(options))
            {
                // ARRANGE
                this.InitializeDB(context);
                var controller = new DeviceController(new Business.DeviceBusiness(new DeviceRepository(context), new UnitOfWork(context)), _mapper);

                // ACT
                var actionResult = await controller.Add(new ViewModels.DeviceViewModel { Name = "Alberto", Brand = "Ma��", CreationTime = DateTime.Now, });

                // ASSERT
                var result = actionResult.Result as CreatedResult;
                Assert.NotNull(result);
                var viewModel = Assert.IsType<ViewModels.DeviceViewModel>(result.Value);
                Assert.Equal(3, viewModel.Id);
            }
        }

        [Fact]
        public async void Get_FilterDevices()
        {
            var options = new DbContextOptionsBuilder<DeviceDbContext>()
            .UseInMemoryDatabase(databaseName: "FilterDevicesDb")
            .Options;

            using (var context = new DeviceDbContext(options))
            {
                // ARRANGE
                this.InitializeDB(context);
                var controller = new DeviceController(new Business.DeviceBusiness(new DeviceRepository(context), new UnitOfWork(context)), _mapper);

                // ACT
                var actionResult = await controller.GetAll(brand: "Mokia");

                // ASSERT
                var result = actionResult.Result as OkObjectResult;
                Assert.NotNull(result);
                var viewModel = Assert.IsAssignableFrom<IEnumerable<ViewModels.DeviceViewModel>>(result.Value);
                Assert.Single(viewModel);
                Assert.Equal("Mokia", viewModel.First().Brand);
            }
        }

        [Fact]
        public async void Get_ReturnsAllDevices()
        {
            var options = new DbContextOptionsBuilder<DeviceDbContext>()
            .UseInMemoryDatabase(databaseName: "GetDevicesDb")
            .Options;

            using (var context = new DeviceDbContext(options))
            {
                // ARRANGE
                this.InitializeDB(context);
                var controller = new DeviceController(new Business.DeviceBusiness(new DeviceRepository(context), new UnitOfWork(context)), _mapper);

                // ACT
                var actionResult = await controller.GetAll();

                // ASSERT
                var result = actionResult.Result as OkObjectResult;
                Assert.NotNull(result);
                var viewModel = Assert.IsAssignableFrom<IEnumerable<ViewModels.DeviceViewModel>>(result.Value);
                Assert.Equal(2, viewModel.Count());
                Assert.Equal("Mokia", viewModel.First().Brand);
            }
        }

        [Fact]
        public async void GetById_NotFound()
        {
            var options = new DbContextOptionsBuilder<DeviceDbContext>()
            .UseInMemoryDatabase(databaseName: "GetByIdNotFoundDevicesDb")
            .Options;

            using (var context = new DeviceDbContext(options))
            {
                // ARRANGE
                this.InitializeDB(context);
                var controller = new DeviceController(new Business.DeviceBusiness(new DeviceRepository(context), new UnitOfWork(context)), _mapper);

                // ACT
                var actionResult = await controller.Get(3);

                // ASSERT
                var result = actionResult.Result as NotFoundObjectResult;
                Assert.NotNull(result);
                Assert.Equal("The device with identifier 3 was not found!", result.Value.ToString());
            }
        }

        [Fact]
        public async void GetById_ReturnDevice()
        {
            var options = new DbContextOptionsBuilder<DeviceDbContext>()
            .UseInMemoryDatabase(databaseName: "GetByIdDevicesDb")
            .Options;

            using (var context = new DeviceDbContext(options))
            {
                // ARRANGE
                this.InitializeDB(context);
                var controller = new DeviceController(new Business.DeviceBusiness(new DeviceRepository(context), new UnitOfWork(context)), _mapper);

                // ACT
                var actionResult = await controller.Get(2);

                // ASSERT
                var result = actionResult.Result as OkObjectResult;
                Assert.NotNull(result);
                var viewModel = Assert.IsType<ViewModels.DeviceViewModel>(result.Value);
                Assert.Equal(2, viewModel.Id);
            }
        }

        [Fact]
        public async void PartialUpdate_BadRequest()
        {
            var options = new DbContextOptionsBuilder<DeviceDbContext>()
            .UseInMemoryDatabase(databaseName: "PartialUpdateFailDevicesDb")
            .Options;

            using (var context = new DeviceDbContext(options))
            {
                // ARRANGE
                this.InitializeDB(context);
                var controller = new DeviceController(new Business.DeviceBusiness(new DeviceRepository(context), new UnitOfWork(context)), _mapper);

                // ACT
                var actionResult = await controller.PartialUpdate(new ViewModels.PatchDeviceViewModel { Name = "Alberto", Brand = "Ma��", });

                // ASSERT
                var result = actionResult.Result as BadRequestObjectResult;
                Assert.NotNull(result);
            }
        }

        [Fact]
        public async void PartialUpdate_Success()
        {
            var options = new DbContextOptionsBuilder<DeviceDbContext>()
            .UseInMemoryDatabase(databaseName: "PartialUpdateSuccessDevicesDb")
            .Options;

            using (var context = new DeviceDbContext(options))
            {
                // ARRANGE
                this.InitializeDB(context);
                var controller = new DeviceController(new Business.DeviceBusiness(new DeviceRepository(context), new UnitOfWork(context)), _mapper);

                // ACT
                var actionResult = await controller.PartialUpdate(new ViewModels.PatchDeviceViewModel { Id = 2, Brand = "Ma��" });

                // ASSERT
                var result = actionResult.Result as OkObjectResult;
                Assert.NotNull(result);
                var viewModel = Assert.IsType<ViewModels.PatchDeviceViewModel>(result.Value);
                Assert.Equal("Manuel", viewModel.Name);
                Assert.Equal("Ma��", viewModel.Brand);
            }
        }

        [Fact]
        public async void Remove_NotFound()
        {
            var options = new DbContextOptionsBuilder<DeviceDbContext>()
            .UseInMemoryDatabase(databaseName: "RemoveFailDevicesDb")
            .Options;

            using (var context = new DeviceDbContext(options))
            {
                // ARRANGE
                this.InitializeDB(context);
                var controller = new DeviceController(new Business.DeviceBusiness(new DeviceRepository(context), new UnitOfWork(context)), _mapper);

                // ACT
                var actionResult = await controller.Remove(3);

                // ASSERT
                Assert.IsType<NotFoundObjectResult>(actionResult);
            }
        }

        [Fact]
        public async void Remove_Success()
        {
            var options = new DbContextOptionsBuilder<DeviceDbContext>()
            .UseInMemoryDatabase(databaseName: "RemoveSuccessDevicesDb")
            .Options;

            using (var context = new DeviceDbContext(options))
            {
                // ARRANGE
                this.InitializeDB(context);
                var controller = new DeviceController(new Business.DeviceBusiness(new DeviceRepository(context), new UnitOfWork(context)), _mapper);

                // ACT
                var actionResult = await controller.Remove(2);

                // ASSERT
                Assert.IsType<NoContentResult>(actionResult);
            }
        }

        [Fact]
        public async void Update_BadRequest()
        {
            var options = new DbContextOptionsBuilder<DeviceDbContext>()
            .UseInMemoryDatabase(databaseName: "UpdateFailDevicesDb")
            .Options;

            using (var context = new DeviceDbContext(options))
            {
                // ARRANGE
                this.InitializeDB(context);
                var controller = new DeviceController(new Business.DeviceBusiness(new DeviceRepository(context), new UnitOfWork(context)), _mapper);

                // ACT
                var actionResult = await controller.Update(new ViewModels.DeviceViewModel { Id = 3, Name = "Alberto", Brand = "Ma��", CreationTime = DateTime.Now, });

                // ASSERT
                var result = actionResult.Result as BadRequestObjectResult;
                Assert.NotNull(result);
            }
        }

        [Fact]
        public async void Update_Success()
        {
            var options = new DbContextOptionsBuilder<DeviceDbContext>()
            .UseInMemoryDatabase(databaseName: "UpdateSuccessDevicesDb")
            .Options;

            using (var context = new DeviceDbContext(options))
            {
                // ARRANGE
                this.InitializeDB(context);
                var controller = new DeviceController(new Business.DeviceBusiness(new DeviceRepository(context), new UnitOfWork(context)), _mapper);

                // ACT
                var actionResult = await controller.Update(new ViewModels.DeviceViewModel { Id = 2, Name = "Alberto", Brand = "Ma��", CreationTime = DateTime.Now, });

                // ASSERT
                var result = actionResult.Result as OkObjectResult;
                Assert.NotNull(result);
                var viewModel = Assert.IsType<ViewModels.DeviceViewModel>(result.Value);
                Assert.Equal("Alberto", viewModel.Name);
            }
        }

        private void InitializeDB(DeviceDbContext context)
        {
            context.Devices.Add(new Models.Device
            {
                Id = 1,
                Name = "Maria",
                Brand = "Mokia",
                CreationTime = DateTime.Now.AddMonths(-23),
            });

            context.Devices.Add(new Models.Device
            {
                Id = 2,
                Name = "Manuel",
                Brand = "Sangsuga",
                CreationTime = DateTime.Now.AddDays(-12),
            });

            context.SaveChanges();
        }
    }
}