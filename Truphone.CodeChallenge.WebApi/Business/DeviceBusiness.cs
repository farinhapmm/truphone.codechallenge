﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Truphone.CodeChallenge.WebApi.Data;
using Truphone.CodeChallenge.WebApi.Models;

namespace Truphone.CodeChallenge.WebApi.Business
{
    public class DeviceBusiness : IDeviceBusiness
    {
        private readonly IDeviceRepository _deviceRepository;
        private readonly IUnitOfWork _unitOfWork;

        public DeviceBusiness(IDeviceRepository deviceRepository, IUnitOfWork unitOfWork)
        {
            _deviceRepository = deviceRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<DeviceResponse> Add(Models.Device device)
        {
            try
            {
                await _deviceRepository.Add(device);
                await _unitOfWork.SaveChanges();

                return new DeviceResponse(device);
            }
            catch (Exception ex)
            {
                // Log exception
                return new DeviceResponse("An error occurred while creating a device");
            }
        }

        public Task<IEnumerable<Device>> Filter(string brand)
        {
            return Task.Run(() => _deviceRepository.Filter(brand));
        }

        public async Task<Models.Device> Get(int id)
        {
            return await _deviceRepository.Get(id);
        }

        public async Task<IEnumerable<Models.Device>> GetAll(int page, int count)
        {
            return await _deviceRepository.GetAll(page, count);
        }

        public async Task<string> Remove(int id)
        {
            var existingDevice = await _deviceRepository.Get(id);
            if (existingDevice == null)
            {
                return $"The device with identifier {id} was not found!";
            }

            _deviceRepository.Remove(existingDevice);
            await _unitOfWork.SaveChanges();
            return null;
        }

        public async Task<DeviceResponse> Update(Device device)
        {
            var existingDevice = await _deviceRepository.Get(device.Id);
            if (existingDevice == null)
            {
                return new DeviceResponse($"The device with identifier { device.Id } was not found!");
            }

            if (device.Brand != null)
            {
                existingDevice.Brand = device.Brand;
            }
            if (device.Name != null)
            {
                existingDevice.Name = device.Name;
            }
            if (device.CreationTime != DateTime.MinValue)
            {
                existingDevice.CreationTime = device.CreationTime;
            }

            try
            {
                _deviceRepository.Update(existingDevice);
                await _unitOfWork.SaveChanges();

                return new DeviceResponse(existingDevice);
            }
            catch (Exception ex)
            {
                // Log exception
                return new DeviceResponse("An error occurred while updating the device");
            }
        }
    }
}