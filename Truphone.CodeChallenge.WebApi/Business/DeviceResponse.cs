﻿namespace Truphone.CodeChallenge.WebApi.Business
{
    public class DeviceResponse
    {
        public Models.Device Device { get; private set; }
        public bool Sucess { get; private set; }
        public string Message { get; private set; }

        private DeviceResponse(bool success, string message, Models.Device device)
        {
            this.Device = device;
            this.Message = message;
            this.Sucess = success;
        }

        public DeviceResponse(Models.Device device) : this(true, string.Empty, device)
        {
        }

        public DeviceResponse(string message) : this(false, message, null)
        {
        }
    }
}