﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Truphone.CodeChallenge.WebApi.Models;

namespace Truphone.CodeChallenge.WebApi.Business
{
    public interface IDeviceBusiness
    {
        Task<DeviceResponse> Add(Models.Device device);

        Task<IEnumerable<Models.Device>> Filter(string brand);

        Task<Models.Device> Get(int id);

        Task<IEnumerable<Models.Device>> GetAll(int page, int count);

        Task<string> Remove(int id);

        Task<DeviceResponse> Update(Device device);
    }
}