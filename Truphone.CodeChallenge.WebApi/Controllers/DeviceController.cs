﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Truphone.CodeChallenge.WebApi.Extensions;

namespace Truphone.CodeChallenge.WebApi.Controllers
{
    [Route("api/devices")]
    [ApiController]
    public class DeviceController : ControllerBase
    {
        private readonly Business.IDeviceBusiness _device;
        private readonly IMapper _mapper;

        public DeviceController(Business.IDeviceBusiness device, IMapper mapper)
        {
            _device = device;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<ActionResult<ViewModels.DeviceViewModel>> Add(ViewModels.DeviceViewModel deviceViewModel)
        {
            var device = _mapper.Map<Models.Device>(deviceViewModel);
            var deviceRsp = await _device.Add(device);

            if (!deviceRsp.Sucess)
            {
                return this.BadRequest(deviceRsp.Message);
            }

            deviceViewModel = _mapper.Map<ViewModels.DeviceViewModel>(deviceRsp.Device);
            return this.Created($"~/api/devices/{deviceViewModel.Id}", deviceViewModel);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Models.Device>> Get(int id)
        {
            var device = await _device.Get(id);
            if (device == null)
            {
                return this.NotFound($"The device with identifier {id} was not found!");
            }

            return this.Ok(_mapper.Map<ViewModels.DeviceViewModel>(device));
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ViewModels.DeviceViewModel>>> GetAll([FromQuery(Name = "brand")] string brand = null, [FromQuery(Name = "page")] int page = default, [FromQuery(Name = "count")] int count = default)
        {
            IEnumerable<Models.Device> devices;
            if (string.IsNullOrEmpty(brand))
            {
                devices = await _device.GetAll(page, count);
            }
            else
            {
                devices = await _device.Filter(brand);
            }

            if (devices != null && devices.Any() && Response != null)
            {
                Response.Headers.Add("X-Total-Count", devices.Count().ToString());
            }
            return this.Ok(_mapper.Map<IEnumerable<ViewModels.DeviceViewModel>>(devices));
        }

        [HttpPatch]
        public async Task<ActionResult<ViewModels.PatchDeviceViewModel>> PartialUpdate(ViewModels.PatchDeviceViewModel deviceViewModel)
        {
            if (deviceViewModel.Id <= 0)
            {
                return BadRequest("The device identifier was not provided or is invalid.");
            }

            var device = _mapper.Map<Models.Device>(deviceViewModel);
            var deviceRsp = await _device.Update(device);

            if (!deviceRsp.Sucess)
            {
                return this.BadRequest(deviceRsp.Device);
            }

            deviceViewModel = _mapper.Map<ViewModels.PatchDeviceViewModel>(deviceRsp.Device);
            return this.Ok(deviceViewModel);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Remove(int id)
        {
            var message = await _device.Remove(id);
            if (message != null)
            {
                return this.NotFound(message);
            }

            return this.NoContent();
        }

        [HttpPut]
        public async Task<ActionResult<ViewModels.DeviceViewModel>> Update(ViewModels.DeviceViewModel deviceViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }
            if (deviceViewModel.Id <= 0)
            {
                return BadRequest("The device identifier was not provided or is invalid.");
            }

            var device = _mapper.Map<Models.Device>(deviceViewModel);
            var deviceRsp = await _device.Update(device);

            if (!deviceRsp.Sucess)
            {
                return this.BadRequest(deviceRsp.Message);
            }

            deviceViewModel = _mapper.Map<ViewModels.DeviceViewModel>(deviceRsp.Device);
            return this.Ok(deviceViewModel);
        }
    }
}