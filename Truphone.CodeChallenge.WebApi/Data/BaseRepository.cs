﻿namespace Truphone.CodeChallenge.WebApi.Data
{
    public abstract class BaseRepository
    {
        protected readonly DeviceDbContext _context;

        public BaseRepository(DeviceDbContext context)
        {
            _context = context;
        }
    }
}