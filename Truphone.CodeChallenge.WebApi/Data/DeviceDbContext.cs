﻿using Microsoft.EntityFrameworkCore;
using Truphone.CodeChallenge.WebApi.Models;

namespace Truphone.CodeChallenge.WebApi.Data
{
    public class DeviceDbContext : DbContext
    {
        public DeviceDbContext(DbContextOptions<DeviceDbContext> options) : base(options)
        {
        }

        public DbSet<Device> Devices { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Device>().ToTable("Device");
            builder.Entity<Device>().HasKey(d => d.Id);
            builder.Entity<Device>().Property(d => d.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Device>().Property(d => d.Name).IsRequired().HasMaxLength(50);
            builder.Entity<Device>().Property(d => d.Brand).IsRequired().HasMaxLength(50);
            builder.Entity<Device>().Property(d => d.CreationTime).IsRequired();
        }
    }
}