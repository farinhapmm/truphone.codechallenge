﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Truphone.CodeChallenge.WebApi.Models;

namespace Truphone.CodeChallenge.WebApi.Data
{
    public class DeviceRepository : BaseRepository, IDeviceRepository
    {
        public DeviceRepository(DeviceDbContext context) : base(context)
        {
        }

        public async Task Add(Device device)
        {
            await _context.Devices.AddAsync(device);
        }

        public IEnumerable<Device> Filter(string brand)
        {
            return _context.Devices.Where(d => d.Brand == brand);
        }

        public async Task<Device> Get(int id)
        {
            return await _context.Devices.FindAsync(id);
        }

        public async Task<IEnumerable<Device>> GetAll(int page, int count)
        {
            return await _context.Devices.Skip(page * count).Take(count).ToListAsync();
        }

        public void Remove(Device device)
        {
            _context.Remove(device);
        }

        public void Update(Device device)
        {
            _context.Devices.Update(device);
        }
    }
}