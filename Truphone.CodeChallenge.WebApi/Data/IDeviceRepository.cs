﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Truphone.CodeChallenge.WebApi.Data
{
    public interface IDeviceRepository
    {
        Task Add(Models.Device device);

        IEnumerable<Models.Device> Filter(string brand);

        Task<Models.Device> Get(int id);

        Task<IEnumerable<Models.Device>> GetAll(int page, int count);

        void Remove(Models.Device existingDevice);

        void Update(Models.Device device);
    }
}