﻿using System.Threading.Tasks;

namespace Truphone.CodeChallenge.WebApi.Data
{
    public interface IUnitOfWork
    {
        Task SaveChanges();
    }
}