﻿using System.Threading.Tasks;

namespace Truphone.CodeChallenge.WebApi.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DeviceDbContext _context;

        public UnitOfWork(DeviceDbContext context)
        {
            _context = context;
        }

        public async Task SaveChanges()
        {
            await _context.SaveChangesAsync();
        }
    }
}