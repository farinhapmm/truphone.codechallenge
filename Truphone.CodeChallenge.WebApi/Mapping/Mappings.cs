﻿using AutoMapper;

namespace Truphone.CodeChallenge.WebApi.Mapping
{
    public class Mappings : Profile
    {
        public Mappings()
        {
            CreateMap<Models.Device, ViewModels.DeviceViewModel>()
                .ReverseMap();
            CreateMap<Models.Device, ViewModels.PatchDeviceViewModel>()
                .ReverseMap();
        }
    }
}