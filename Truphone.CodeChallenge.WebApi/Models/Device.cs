﻿using System;

namespace Truphone.CodeChallenge.WebApi.Models
{
    public class Device
    {
        public string Brand { get; set; }
        public DateTime CreationTime { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}