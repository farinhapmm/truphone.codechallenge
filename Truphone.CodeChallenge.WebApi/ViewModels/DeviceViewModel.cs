﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Truphone.CodeChallenge.WebApi.ViewModels
{
    public class DeviceViewModel
    {
        [Required]
        [MaxLength(50)]
        public string Brand { get; set; }

        [Required]
        public DateTime CreationTime { get; set; }

        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
    }
}