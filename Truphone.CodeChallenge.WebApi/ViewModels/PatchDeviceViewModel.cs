﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Truphone.CodeChallenge.WebApi.ViewModels
{
    public class PatchDeviceViewModel
    {
        [MaxLength(50)]
        public string Brand { get; set; }

        public DateTime CreationTime { get; set; }

        [Required]
        public int Id { get; set; }

        [MaxLength(50)]
        public string Name { get; set; }
    }
}